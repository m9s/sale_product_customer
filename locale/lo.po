#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:product.product,product_customers:"
msgid "Customers"
msgstr ""

msgctxt "field:product.template,product_customers:"
msgid "Customers"
msgstr ""

msgctxt "field:sale.line,product_customer:"
msgid "Customer's Product"
msgstr ""

msgctxt "field:sale.product_customer,code:"
msgid "Code"
msgstr ""

msgctxt "field:sale.product_customer,create_date:"
msgid "Create Date"
msgstr ""

msgctxt "field:sale.product_customer,create_uid:"
msgid "Create User"
msgstr ""

msgctxt "field:sale.product_customer,id:"
msgid "ID"
msgstr ""

msgctxt "field:sale.product_customer,name:"
msgid "Name"
msgstr ""

msgctxt "field:sale.product_customer,party:"
msgid "Customer"
msgstr ""

msgctxt "field:sale.product_customer,product:"
msgid "Variant"
msgstr ""

msgctxt "field:sale.product_customer,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:sale.product_customer,sequence:"
msgid "Sequence"
msgstr ""

msgctxt "field:sale.product_customer,template:"
msgid "Product"
msgstr ""

msgctxt "field:sale.product_customer,write_date:"
msgid "Write Date"
msgstr ""

msgctxt "field:sale.product_customer,write_uid:"
msgid "Write User"
msgstr ""

msgctxt "model:ir.action,name:act_product_customer_form"
msgid "Product Customers"
msgstr ""

msgctxt "model:ir.ui.menu,name:menu_product_customer"
msgid "Product Customers"
msgstr ""

msgctxt "model:sale.product_customer,name:"
msgid "Product Customer"
msgstr ""
